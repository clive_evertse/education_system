<?php

class User_model extends CI_Model {

	//Validates if user exists
	function validate($email,$password)
	{
		$this->db->where('email',$email );
		$this->db->where('password', md5($password));
		$query = $this->db->get('user');
		
		if($query->num_rows == 1)
		{
			return $query->row_array();
		}
		
	}
	//Get specific user details
	function get_user($user_id)
	{
		$this->db->where('user.id',$user_id);
		return $this->db->select('user.id,user.email,teachers.first_name,teachers.last_name,teachers.contact_info,teachers.cell_number')
            ->from('user')
            ->join('teachers', 'user.id = teachers.id','left')
            ->get()->row_array();
	}
	//Gets all user from user table
	function get_all_parents()
	{
		$query = $this->db->select('user.id,parents.first_name as name,parents.last_name,user.email,user.last_activity,user.role')->from('user')->join('parents', 'user.id = parents.id','left')->where('user.role','parent')->get();
		if($query->num_rows >= 1)
		{
			$record_count = $query->num_rows;
			$data['records'] = $query->result_array();
			$data['record_count'] = $record_count;
			return $data;
		}
	}

	function get_all_teachers()
	{
		$query = $this->db->select('user.id,teachers.first_name as name,teachers.last_name,user.email,user.last_activity,user.role')->from('user')->join('teachers', 'user.id = teachers.id','left')->where('user.role','teacher')->get();

		if($query->num_rows >= 1)
		{
			$record_count = $query->num_rows;
			$data['records'] = $query->result_array();
			$data['record_count'] = $record_count;
			return $data;
		}
	}
	
	//Inserts user into user table
	function create_user($name,$surname,$email,$password,$role)
	{


		$user_data = array(
			'email' => $email ,
			'role' => $role ,
			'password' => md5($password),
			'role'=> $role,
			'last_activity'=> date('Y-m-d')
		);
		$this->db->insert('user',$user_data);

		$role_data = array(
			'id' => $this->db->insert_id(),
			'first_name' => $name,
			'last_name' => $surname
		);

		if ($role == 1)
		{
			return $this->db->insert('parents',$role_data);
		}
		elseif ($role == 1)
		{
			return $this->db->insert('teachers',$role_data);
		}
	}
	
	//Checks what fields are being sent, and updates them to user table
	function edit_user($user_data)
	{
		$this->db->where('id',$user_data['id']);
		if ($user_data['name'] != '')
			{
				$this->db->set('name',$user_data['name']);
			}
		if ($user_data['surname'] != '')
			{
				$this->db->set('surname',$user_data['surname']);
			}
		if ($user_data['email'] != '')
			{
				$this->db->set('email',$user_data['email']);
			}
		if ($user_data['password'] != '')
			{
				$this->db->set('password',md5($user_data['password']));
			}
			
		$this->db->update('user');
	}
	
	//Removes user from user table
	function delete_user($user_id)
	{
		$this->db->where('id',$user_id);
		$this->db->delete('user');
	}
	
	//Updates User activity
	function update_user_activity()
	{
		$update_member_activity = array (
		'last_activity' => date('Y-m-d')
		);
		$this->db->where('email',$this->session->userdata('email'));
		$this->db->update('user',$update_member_activity);
	}
    
    public function get_all_students($parent_id)
    {
        $this->db->where('parent',$parent_id);
        $query = $this->db->get('student');
		if($query->num_rows >= 1)
		{
			return $query->result_array();
		}
    }
	
}