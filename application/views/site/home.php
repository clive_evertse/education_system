<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
		<title>Home</title>
		<link rel="stylesheet" href="<?php echo base_url();?>assests/css/style.css" media="screen" type="text/css" />
	</head>
	<body>
		<div id="content">
			<h1>Parents</h1> <div align = "right"><a href = "<?php echo base_url();?>login/logout" >Logout</a></div>
			<p><a href="<?php echo base_url();?>user/add/">Add User</a></p>
<?php
if (isset($action))
	{
		if($action == 'edited')
		{
			echo '<p class = "isa_success"> User has been successfully Edited! </p>';
		}
		if($action == 'deleted')
		{
			echo '<p class = "isa_success"> User has been successfully Deleted! </p>';
		}
		
	}?>
			<table>
			<thead>
				<tr>
					<th>Name</th>
					<th>Surname</th>
					<th>Email</th>
					<th>Last Active</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($parents['records'] as $parent): ?>
				<tr>
				<td><?php echo $parent['name'];?></td>
				<td><?php echo $parent['last_name'];?></td>
				<td><?php echo $parent['email'];?></td>
				<td><?php echo $parent['last_activity'];?></td>
				<?php if ($parent['role'] != 'admin')
					{
						echo '<td><a href="'.base_url().'user/delete_user/'.$parent['id'].'">Delete</a><a href="'. base_url().'user/edit_user/'. $parent['id'].'"> Edit</a></td>';
					}
				?>
				</tr>
			<?php endforeach; ?>
			  </tbody> 
			</table>


		<h1>Teacher</h1> <div align = "right"><a href = "<?php echo base_url();?>login/logout" >Logout</a></div>
		<p><a href="<?php echo base_url();?>user/add/">Add User</a></p>

		<table>
			<thead>
			<tr>
				<th>Name</th>
				<th>Surname</th>
				<th>Email</th>
				<th>Last Active</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($teachers['records'] as $teacher): ?>
				<tr>
					<td><?php echo $teacher['name'];?></td>
					<td><?php echo $teacher['last_name'];?></td>
					<td><?php echo $teacher['email'];?></td>
					<td><?php echo $teacher['last_activity'];?></td>
					<?php if ($teacher['role'] != 'admin')
					{
						echo '<td><a href="'.base_url().'user/delete_user/'.$teacher['id'].'">Delete</a><a href="'. base_url().'user/edit_user/'. $teacher['id'].'"> Edit</a></td>';
					}
					?>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		</div>
	</body>
</html>
