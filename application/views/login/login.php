<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Log-in</title>
		<link rel='stylesheet' href='http://codepen.io/assets/libs/fullpage/jquery-ui.css'>
		<link rel="stylesheet" href="<?php echo base_url();?>assests/css/style.css" media="screen" type="text/css" />
	</head>
	<body>
		<div class="login-card">
			<h1>Log-in</h1><br>
			<?php echo validation_errors('<p class="isa_error">'); ?>
			<?php 

			if (isset($error))
				{
				if ($error == 'error')
				{
					echo '<p class = "isa_error" >Email or Password was incorrect</p>';
				}

				if ($error == 'not_logged_in')
				{
					echo '<p class = "isa_warning" >Please Login to view this page</p>';
				}
			}?>

			<?php echo form_open('login/validate_credentials'); ?>
			<input type="text" name="email" placeholder="Email">
			<input type="password" name="password" placeholder="Password">
			<input type="submit" name="login" class="login login-submit" value="login">
			</form>
		</div>

		<!-- <div id="error"><img src="https://dl.dropboxusercontent.com/u/23299152/Delete-icon.png" /> Your caps-lock is on.</div> -->

		<script src='http://codepen.io/assets/libs/fullpage/jquery_and_jqueryui.js'></script>

	</body>

</html>