<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
		<title>Home</title>
		<link rel="stylesheet" href="<?php echo base_url();?>assests/css/style.css" media="screen" type="text/css" />
	</head>
	<body>
		<div id="content">
			<h1>Users</h1> <div align = "right"><a href = "<?php echo base_url();?>login/logout" >Logout</a></div>
			<p><a href="<?php echo base_url();?>user/add/">Add User</a></p>
<?php
if (isset($action))
	{
		if($action == 'edited')
		{
			echo '<p class = "isa_success"> Child has been successfully Edited! </p>';
		}
		if($action == 'deleted')
		{
			echo '<p class = "isa_success"> Child has been successfully Deleted! </p>';
		}
		
	}?>
			<table>
			<thead>
				<tr>
					<th>Name</th>
					<th>Surname</th>
					<th>Contact Info</th>
					<th>Subject</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($users as $user): ?>
				<tr>
				<td><?php echo $user['first_name'];?></td>
				<td><?php echo $user['last_name'];?></td>
				<td><?php echo $user['contact_info'];?></td>
				<td><?php echo $user['subject'];?></td>
				</tr>
			<?php endforeach; ?>
			  </tbody> 
			</table>
		</div>
	</body>
</html>
