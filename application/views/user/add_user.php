<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Add User</title>
		<link rel="stylesheet" href="<?php echo base_url();?>assests/css/style.css" media="screen" type="text/css" />
	</head>
	<body>
		<div id="content">
			<h1>Add a User</h1>
			<?php
				if (isset($action))
				{
					echo '<p class = "isa_success"> User has been successfully created! </p>';
				}?>
				
			<?php echo validation_errors('<p class="isa_error">'); ?>
		<?php echo  form_open('user/save');?>
			<table border="0" cellpadding="4" cellspacing="0">
				<tbody>
					<tr><td><b> *** Add a new User***</b></td></tr>
					<tr><td><strong>First Name </strong></td><td><?php echo form_input('name');?></td></tr>
					<tr><td><strong>Last Name</strong></td><td><?php echo form_input('surname');?></td></tr>
					<tr><td><strong>Email</strong></td>><td><?php echo form_input('email');?></td></tr>
					<tr><td><strong>Password</strong></td><td><input type="password" name="password" placeholder="Password"></td><td></tr>
					<tr><td><strong>Confirm Password</strong></td><td><?php echo form_password('confirm_password');?></td></tr>
				</tbody>
			</table>
		<?php echo form_submit('','Add User');?>
		</form>
	</div>
	</body>
</html>
