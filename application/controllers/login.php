<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form'));
		$this->load->model('user_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$this->load->view('login/login');
	}
	
	//Validates the user signing in
	function validate_credentials()
	{
		
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		
		$query = $this->user_model->validate($email,$password);
		
		 // if the user's credentials validated
		if ($query)
		{
			$data = array(
				'email' => $this->input->post('email'),
				'is_logged_in' => true,
                'id' => $query['id']
			);

			if ($query['role'] == 'teacher')
			{
				$this->session->set_userdata($data);
				redirect('teachers/home');
			}

			if ($query['role'] == 'parent')
			{
				$this->session->set_userdata($data);
				redirect('parents/home');
			}

			if ($query['role'] == 'student')
			{
				$this->session->set_userdata($data);
				redirect('students/home');
			}

			if ($query['role'] == 'admin')
			{
				$this->session->set_userdata($data);
				redirect('site/home');
			}


		}
		// incorrect username or password
		else 
		{
			$data['error'] = 'error';
			$this->load->view('login/login', $data);
		}
	}
	//User not logged in, but trying to view contents on other controllers
	function not_logged_in()
	{
		$data['error'] = 'not_logged_in';
		$this->load->view('login/login',$data);
	}
	
	//Logout, destroys session
	function logout()
	{
		
		$this->user_model->update_user_activity();
		$array_items = array('email' => '');
		$this->session->unset_userdata($array_items);
		$this->session->sess_destroy();
		redirect('login');
	}
}