<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->is_logged_in();
		$this->load->helper(array('form'));
		$this->load->model('user_model');
	}
	
	//Checks to see if session has been started and if user is logged in
	function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			redirect('login/not_logged_in');
		}		
	}
	
	//Home page or Index method for Site Controller
	public  function home()
	{
		$data['action'] = $this->session->flashdata('action');
		$data['parents'] = $this->user_model->get_all_parents();
		$data['teachers'] = $this->user_model->get_all_teachers();
		$this->load->view('site/home',$data);
	}
}