<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parents extends CI_Controller {

    function __construct()
    {
        parent::__construct();
	    $this->is_logged_in();
	    $this->load->helper(array('form'));
	    $this->load->model('user_model');
    }

	//Checks to see if session has been started and if user is logged in
	function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			redirect('login/not_logged_in');
		}
	}

	//Home page or Index method for Parent Controller
	public  function home()
	{
        $parent_id = $this->session->userdata('id');
        //die($parent_id);
		$data['action'] = $this->session->flashdata('action');
		$data['users'] = $this->user_model->get_all_students($parent_id);
        //print_r($data['users']);die();
		$this->load->view('parents/home',$data);
	}
}
/* End of file parents.php */
/* Location: ./application/controllers/parents.php */