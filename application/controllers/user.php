<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->is_logged_in();
		$this->load->helper(array('form'));
		$this->load->model('user_model');
	}
	
	//Check if session is started
	function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			redirect('login/not_logged_in');
		}		
	}
	
	//Loads Add User view for admin to add new user
	public  function add()
	{
		$this->load->view('site/add');
	}
	
	//New User information is sent to this method for validation and saved if validation is passed
	public  function save()
	{
		$this->load->library('form_validation');

		// field name, error message, validation rules
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('surname', 'Surname', 'trim|required');
		$this->form_validation->set_rules('role', 'Role', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'strip_tags|trim|required|valid_email|is_unique[user.email]');
		$this->form_validation->set_rules('password', 'Password', 'strip_tags|trim|required|min_length[6]|max_length[32]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'strip_tags|trim|required|matches[password]');
		
		//Validation has failed
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('site/add');
		}
		
		//Validation has passed
		else
		{
			$name = $this->input->post('name');
			$surname = $this->input->post('surname');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$role = $this->input->post('role');
			$this->user_model->create_user($name,$surname,$email,$password,$role);
			$data['action'] = 'success';
			$this->load->view('site/add',$data);
		}
	}
	
	//Confirm if you want to remove user
	public  function delete_user($user_id)
	{
		$data['user'] = $this->user_model->get_user($user_id);
		$this->load->view('user/delete_user',$data);
	}
	
	//Removes user from user table
	public function remove_user($user_id)
	{
		$this->user_model->delete_user($user_id);
		$this->session->set_flashdata('action', 'deleted' );
		redirect('site/home');
	}
	//Edit the user
	public function save_edit()
	{
		//Checks if password is null, then does validation on password
		if ($this->input->post('password') != '')
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('password', 'Password', 'strip_tags|trim|required|min_length[6]|max_length[32]');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'strip_tags|trim|required|matches[password]');
			
			//Form validation
			if ($this->form_validation->run() == FALSE)
				{
					$data['user'] = $this->user_model->get_user($this->input->post('id'));
					$this->load->view('user/edit_user',$data);
				}
			
			else
			{
				$this->user_model->edit_user($this->input->post());
				$this->session->set_flashdata('action', 'edited' );
				redirect('site/home');
			}
		}


	}
	
	//Saves the edited fields to user table
	public  function edit_user($user_id)
	{
		$data['user'] = $this->user_model->get_user($user_id);
		$this->load->view('user/edit_user',$data);
	}
}